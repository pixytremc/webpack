/**
 * Created by ira on 8/15/17.
 */

module.exports = function(){
    return {
        devServer: {
            stats: 'errors-only',
            port: 9000
        }
    }
};
