/**
 * Created by ira on 8/15/17.
 */

module.exports = function(paths){
    return {
        module: {
            rules: [
                {
                    test: /\.sass$/,
                    include: paths,
                    use: [
                        'style-loader',
                        'css-loader',
                        'sass-loader'
                    ]
                }
            ]
        }
    };
};
