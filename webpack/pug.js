/**
 * Created by ira on 8/15/17.
 */

module.exports = function(){
    return {
        module: {
            rules: [
                {
                    test: /\.pug$/,
                    loader: 'pug-loader',
                    options: {
                        pretty: true
                    }
                }
            ]
        }
    }
};
