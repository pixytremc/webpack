/**
 * Created by ira on 8/15/17.
 */

const webpack = require('webpack');

module.exports = function() {
    return {
        plugins: [
            new webpack.optimize.UglifyJsPlugin({
                sourceMap: true,
                compress: {
                    warnings: false
                }
            })
        ]
    };
};
