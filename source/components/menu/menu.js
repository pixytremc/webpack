/**
 * Created by ira on 8/10/17.
 */

import './menu.sass';
import 'normalize.css';

export default function(array, className){
    var menu = document.createElement("ul");
    menu.className = className;
    var listItems = '';
    array.forEach(function(item){
        listItems += '<li>'+item+'</li>';
    });
    menu.innerHTML = listItems;
    return menu;
}
